package se.wifi.scheresteinpapier;
import java.util.Scanner;

/**
 * Beschreib ein Spiel mit zwei Spielern und von aussen mitgegebenen Spiellogic
 * 
 * @author brunnhoferl
 *
 */

public class Spiel {
	/** VARIABLEN **/
	
	Spieler spieler1;
	Auswahl spieler1Auswahl;
	Spieler spieler2;
	Auswahl spieler2Auswahl;
	ISpielLogik schereSteinPapierLogik;
	
	int anzahlRunden = 0;
	int bestOf = 3;
	int drawCount = 0;
	
	/**
	 * @param spieler1 
	 * @param spieler1Auswahl
	 * @param spieler2
	 * @param spieler2Auswahl
	 */
	public Spiel(Spieler spieler1, Auswahl spieler1Auswahl, Spieler spieler2,
			Auswahl spieler2Auswahl, ISpielLogik spielLogic) {
		this.spieler1 = spieler1;
		this.spieler1Auswahl = spieler1Auswahl;
		this.spieler2 = spieler2;
		this.spieler2Auswahl = spieler2Auswahl;
		this.schereSteinPapierLogik = spielLogic;
	}
	
	/**
	 * @param spieler1 
	 * @param spieler1Auswahl
	 * @param spieler2
	 * @param spieler2Auswahl
	 */
	public Spiel(Spieler spieler1,  Spieler spieler2,
			 ISpielLogik spielLogic) {
		this.spieler1 = spieler1;
		this.spieler2 = spieler2;
		this.schereSteinPapierLogik = spielLogic;
	}
	
	/** GETTER & SETTER
	 */
	public Spieler getSpieler1() {
		return spieler1;
	}
	public void setSpieler1(Spieler spieler1) {
		this.spieler1 = spieler1;
	}
	public Auswahl getSpieler1Auswahl() {
		return spieler1Auswahl;
	}
	public void setSpieler1Auswahl(Auswahl spieler1Auswahl) {
		this.spieler1Auswahl = spieler1Auswahl;
	}
	public Spieler getSpieler2() {
		return spieler2;
	}
	public void setSpieler2(Spieler spieler2) {
		this.spieler2 = spieler2;
	}
	public Auswahl getSpieler2Auswahl() {
		return spieler2Auswahl;
	}
	public void setSpieler2Auswahl(Auswahl spieler2Auswahl) {
		this.spieler2Auswahl = spieler2Auswahl;
	}
	
	/**
	 * FUNKTION
	 */
	
	public void spielen(){
		
		Spieler gewinner;
		String userEingabe;
		AuswahlTreffen auswahlTreffen;
		Auswahl spieler2Auswahl;
		Auswahl spieler1Auswahl;
		Scanner scanner;
		scanner= new Scanner(System.in);
		auswahlTreffen = new AuswahlTreffen();
	
	
		for(anzahlRunden=1;anzahlRunden<=bestOf;anzahlRunden++){
			System.out.println("Runde "+anzahlRunden+" of " +bestOf);
			//Userauswahl treffen
			System.out.println("W�hle (S)chere, S(T)ein, (P)apier, (E)chse oder (SP)ock aus!");
			userEingabe = scanner.nextLine();
			spieler1Auswahl = auswahlTreffen.calculateSpielerAuswahl(userEingabe);
			
			//Computerauswahl mit Hilfe der Hilfsklasse AuswahlTreffen treffen
			spieler2Auswahl = auswahlTreffen.calculateComputerAuswahl();
			
			//Variante 1
//			gewinner = schereSteinPapierLogik.ermittleGewinner(spieler1, spieler1Auswahl, spieler2, spieler2Auswahl);
			
			gewinner = schereSteinPapierLogik.ermittleGewinnerOhneStringVergleich(spieler1, spieler1Auswahl, spieler2, spieler2Auswahl);
			
			//Variante 1
//			if(gewinner.getName().equals(spieler1.getName())){
//				spieler1.inkrAnzGewonnen();
//			}else if (gewinner.getName().equals(spieler2.getName())){
//				spieler2.inkrAnzGewonnen();
//			} else {
//				drawCount++;
//			}
			
			if(gewinner.getName().equals("KEIN_GEWINNER")){
				drawCount++;
			}
			
			
			System.out.println(spieler1.getName() + " hat "+spieler1Auswahl.getBez()+ " gew�hlt!");
			System.out.println(spieler2.getName() + " hat "+spieler2Auswahl.getBez()+ " gew�hlt!");
			System.out.println("\n"+gewinner.getName() +" hat das Spiel gewonnen");			
		}
		
		System.out.println("Siege "+spieler1.getName()+ " "+spieler1.getAnzGewonnen()+"\n"
				+"Siege "+spieler2.getName()+ " "+spieler2.getAnzGewonnen()+"\n"
				+"Anzahl der Unentschieden "+drawCount);
		
		if(spieler1.getAnzGewonnen()<spieler2.getAnzGewonnen()){
			System.out.println(spieler2.getName()+ " ist Gesamtsieger!");
		} else if(spieler1.getAnzGewonnen()>spieler2.getAnzGewonnen()){
			System.out.println(spieler1.getName()+ " ist Gesamtsieger!");
		} else {
			System.out.println("Kein Gewinner!");
		}
		
		scanner.close();

	}
}
