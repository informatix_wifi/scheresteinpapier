package se.wifi.scheresteinpapier;
/**
 * Auswahl Schere
 * 
 * @author brunnhoferl
 *
 * 
 */
public class Schere extends Auswahl {

	/*
	 * CONSTURCTORS
	 */
	
	public Schere(){
		bez = "schere";
	}
	
	public boolean schlaegt(Auswahl a) {
		if(a.getBez().equals("echse")||a.getBez().equals("papier")){
			return true;
		} else {
			return false;			
		}
	}
	
}
