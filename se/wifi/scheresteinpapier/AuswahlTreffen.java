package se.wifi.scheresteinpapier;
/**
 * 
 * @author brunnhoferl
 *
 */
public class AuswahlTreffen {

	/**
	 * Wandelt String userInput in ein Auswahlobjekt um.
	 * Erwarteter Wert ist S,T,E,SP oder P
	 * Input�berpr�fung ausserhalb der Methode vornehmen.
	 * 
	 * @param userInput = S,T,E, SP oder P
	 * @return Auswahl Schere, Stein oder Papier, Echse, Spock
	 */
	public Auswahl calculateSpielerAuswahl(String userInput){
		
		if(userInput.equals("S")){
			return new Schere();
		} else if(userInput.equals("T")){
			return  new Stein();
		}else if(userInput.equals("P")){
			return new Papier();
		}else if(userInput.equals("E")){
			return new Echse();
		}else {
			return new Spock();
		}
	}
	
	/**
	 * Zuf�llige Auswahl des Computers
	 * 
	 * @return Auswahl Object Stein, Schere oder Papier, Spock, Echse
	 */
	public Auswahl calculateComputerAuswahl(){
		double random = Math.random();
		Auswahl auswahlSpieler2;
		if(random<=0.2){
//			System.out.println("Computer hat Schere ausgew�hlt!");
			auswahlSpieler2 = new Schere();
		} else if(random>0.2 && random <=0.4){
//			System.out.println("Computer hat Stein ausgew�hlt!");
			auswahlSpieler2 = new Stein();
		} else if(random>0.4 && random <=0.6){
//			System.out.println("Computer hat Papier ausgew�hlt!");
			auswahlSpieler2 = new Papier();
		}else if(random>0.6 && random <=0.8){
//			System.out.println("Computer hat Echse ausgew�hlt!");
			auswahlSpieler2 = new Echse();
		} else {
//			System.out.println("Computer hat Spock ausgew�hlt!");
			auswahlSpieler2 = new Spock();
		}
		
		return auswahlSpieler2;
	}
	
}
