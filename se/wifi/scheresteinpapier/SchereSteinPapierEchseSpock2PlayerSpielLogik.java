package se.wifi.scheresteinpapier;
/**
 * 
 * @author brunnhoferl
 *
 */
public class SchereSteinPapierEchseSpock2PlayerSpielLogik implements ISpielLogik {

	/**
	 * 
	 * @deprecated weil nicht auf Echse Spock erweitert
	 */
	public Spieler ermittleGewinner(Spieler spieler1, Auswahl auswahlspieler1, Spieler spieler2, Auswahl auswahlspieler2){
		/*
		 * Hier wird der Gewinner emittelt
		 */

		// Spieler1 gewinnt
		if(
				(auswahlspieler1.getBez().equals("schere") && auswahlspieler2.getBez().equals("papier")) ||
				(auswahlspieler1.getBez().equals("stein") && auswahlspieler2.getBez().equals("schere")) ||
				(auswahlspieler1.getBez().equals("papier") && auswahlspieler2.getBez().equals("stein"))
				)
		{
			return spieler1;
		}

		// Spieler2 gewinnt
		else if(
				(auswahlspieler1.getBez().equals("papier") && auswahlspieler2.getBez().equals("schere")) ||
				(auswahlspieler1.getBez().equals("schere") && auswahlspieler2.getBez().equals("stein")) ||
				(auswahlspieler1.getBez().equals("stein") && auswahlspieler2.getBez().equals("papier"))
				)
		{
			return spieler2;
		}

		// UNENTSCHIEDEN
		else if(auswahlspieler1.vergleiche(auswahlspieler2))
		{
			// neues Object "spieler_unentschieden" mit dem Namen "KEIN_GEWINNER";
			Spieler spieler_unentschieden = new Spieler();
			spieler_unentschieden.setName("KEIN_GEWINNER");
			return spieler_unentschieden;

		}
		else
		{
			System.out.println("Class '" + getClass().getName() + "': ERROR - Falscher Parameter/Wert");
			System.out.println("   auswahlspieler1.getBez() '" + auswahlspieler1.getBez() + "'");
			System.out.println("   auswahlspieler2.getBez() '" + auswahlspieler2.getBez() + "'");

			// neues Object "WFT" mit dem Namen "WFT.... ERROR";
			Spieler WFT = new Spieler();
			WFT.setName("WFT.... ERROR");
			return WFT;			
		}
	}
	
	/**
	 * 
	 */

	public Spieler ermittleGewinnerOhneStringVergleich(Spieler spieler1, Auswahl auswahlspieler1, Spieler spieler2, Auswahl auswahlspieler2){
		if(auswahlspieler1.schlaegt(auswahlspieler2)){
			spieler1.inkrAnzGewonnen();
			return spieler1;
		} else if(auswahlspieler2.schlaegt(auswahlspieler1)){
			spieler2.inkrAnzGewonnen();
			return spieler2;
		} else {
			Spieler spieler_unentschieden = new Spieler();
			spieler_unentschieden.setName("KEIN_GEWINNER");
			return spieler_unentschieden;
		}
	}	
}
