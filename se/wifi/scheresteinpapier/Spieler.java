package se.wifi.scheresteinpapier;
/**
 * 
 * Beschreibt einen Spieler in unserem Spiel.
 * 
 * @author brunnhoferl
 *
 */
public class Spieler {
	
	private String name;
	private boolean mensch;
	private int anzGewonnen = 0;
	

	/**Konstruktoren**/
	public Spieler(){
		
	}
	
	public Spieler(String name){
		this.name = name;
	}
	
	public Spieler(String name, boolean isMensch){
		this.name = name;
		this.mensch = isMensch;
	}
	
	/** GETTER &  SETTER **/
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isMensch() {
		return mensch;
	}
	public void setMensch(boolean mensch) {
		this.mensch = mensch;
	}
	
	public int getAnzGewonnen() {
		return anzGewonnen;
	}

	public void setAnzGewonnen(int anzGewonnen) {
		this.anzGewonnen = anzGewonnen;
	}
	
	
	/** FUNCTIONS **/
	
	public void inkrAnzGewonnen(){
		this.anzGewonnen++;
	}

}
