package se.wifi.scheresteinpapier;
import java.util.Scanner;

/**
 * Start Klasse f�r Schere Stein Papier Echse Spock
 * @author brunnhoferl
 *
 */
public class StarteSpiel {

	public static void main(String[] args) {
		/** Variabel Definition **/
		
		Spieler spieler1;
		Spieler computer1;
		Scanner scanner;
		ISpielLogik spielLogik;
		Spiel spiel;
		spielLogik = new SchereSteinPapierEchseSpock2PlayerSpielLogik();
		spieler1 = new Spieler();
		computer1 = new Spieler("Sheldon Artifical Intelligence", false);
		String userEingabe;
				
		//PROGRAMM START
		scanner = new Scanner(System.in);
		
		
		//Spieler Namen einlesen und setzen
		System.out.println("Bitte Name eingeben: ");
		scanner = new Scanner(System.in);
		userEingabe = scanner.nextLine();
		
		spieler1.setName(userEingabe);
		spieler1.setMensch(true);
		
	
		//Spiel initalisieren
		spiel = new Spiel(spieler1, computer1, spielLogik);
		
		//Starte Spiel
		spiel.spielen();
		
		scanner.close();
	}
	
}
