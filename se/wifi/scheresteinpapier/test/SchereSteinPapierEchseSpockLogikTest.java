package se.wifi.scheresteinpapier.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import se.wifi.scheresteinpapier.Auswahl;
import se.wifi.scheresteinpapier.Echse;
import se.wifi.scheresteinpapier.Schere;
import se.wifi.scheresteinpapier.SchereSteinPapierEchseSpock2PlayerSpielLogik;
import se.wifi.scheresteinpapier.Spieler;
import se.wifi.scheresteinpapier.Spock;
import se.wifi.scheresteinpapier.Stein;

public class SchereSteinPapierEchseSpockLogikTest {
	Spieler mensch,computer, gewinner;
	Auswahl spieler1Auswahl,spieler2Auswahl;
	SchereSteinPapierEchseSpock2PlayerSpielLogik logik;
	
	@Before
	public void setup(){
		mensch = new Spieler();
		spieler1Auswahl = new Schere();
		mensch.setName("Mario");
		
		computer = new Spieler("Sheldon",false);
		spieler2Auswahl = new Schere();
				
		logik = new SchereSteinPapierEchseSpock2PlayerSpielLogik();
		
		
		
	}
	
	@Ignore
	@Test
	public void test() {
		fail("Not yet implemented");
	}

	/**
	 * testet R�ckgabewert 
	 */
	@Test
	public void test01selbeAuswahl(){
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, spieler1Auswahl, computer, spieler2Auswahl);
		assertEquals("Spieler1 und Computer w�hlen Schere, Unentschieden!","KEIN_GEWINNER", gewinner.getName());

	}
	
	/**
	 * 
	 */	
	@Test
	public void test02Spieler1gewinnt(){
		Auswahl spieler1Echse = new Echse();
		Auswahl spieler2Spock = new Spock();
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, spieler1Echse, computer, spieler2Spock);
		assertEquals("Spieler 1 w�hlt Echse, Computer w�hlt Spock. Spieler1 gewinnt!","Mario",gewinner.getName());			
	}
	
	/**
	 * 
	 */	
	@Test
	public void test031Spieler1verliert(){
		Auswahl spieler1Schere = new Schere();
		Auswahl spieler2Spock = new Spock();
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, spieler1Schere, computer, spieler2Spock);
		assertEquals("Spieler 1 w�hlt Schere, Computer w�hlt Spock. Spieler2 gewinnt!","Sheldon",gewinner.getName());			
	}
	
	/**
	 * 
	 */	
	@Test
	public void test032Spieler1verliert(){
		Auswahl spieler1Schere = new Schere();
		Auswahl spieler2Stein = new Stein();
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, spieler1Schere, computer, spieler2Stein);
		assertEquals("Spieler 1 w�hlt Schere, Computer w�hlt Stein. Spieler2 gewinnt!","Sheldon",gewinner.getName());			
	}
	
	
}
