package se.wifi.scheresteinpapier;
/**
 * SUPER KLASSE f�r Schere, Stein, Papier
 * @author brunnhoferl
 *
 */
public abstract class Auswahl {
	
	protected String bez;

	/*
	 * CONSTURCTORS
	 */
	
	
	/*
	 * GETTERS AND SETTERS
	 */
	public String getBez() {
		return bez;
	}

	public void setBez(String bez) {
		this.bez = bez;
	}
	
	/*
	 * FUNCTIONS 
	 */
	
	/**
	 * Vergleicht zwei Auswahl Objekte liefert true, falls beide
	 * vom gleichen Typen sind, ansonsten false.
	 * @param a Typ Auswahl
	 * @return true ... this ist gleich a (Bsp: this = Stein, a = Stein)
	 * false ... this ist ungleich a (Bsp: this = Stein, a = Schere)
	 */
	public boolean vergleiche(Auswahl a){
		if(this.bez.equals(a.bez)){
			return true;
		} else {
			return false;
		}
	}
	
	abstract public boolean schlaegt(Auswahl a);
}
