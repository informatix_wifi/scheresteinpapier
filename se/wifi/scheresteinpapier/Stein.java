package se.wifi.scheresteinpapier;
/**
 * Auswahl Stein
 * @author brunnhoferl
 *
 */
public class Stein extends Auswahl {

	/*
	 * CONSTURCTORS
	 */
	public Stein(){
		bez = "stein";
	}
	
	public boolean schlaegt(Auswahl a) {
		if(a.getBez().equals("echse")||a.getBez().equals("schere")){
			return true;
		} else {
			return false;			
		}
	}
}
