package se.wifi.scheresteinpapier;

public interface ISpielLogik {

	public Spieler ermittleGewinner(Spieler spieler1, Auswahl auswahlspieler1, Spieler spieler2, Auswahl auswahlspieler2);
	
	public Spieler ermittleGewinnerOhneStringVergleich(Spieler spieler1, Auswahl auswahlspieler1, Spieler spieler2, Auswahl auswahlspieler2);

}
