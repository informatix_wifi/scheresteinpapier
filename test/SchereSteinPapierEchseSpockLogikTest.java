package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import meineFiles.Auswahl;
import meineFiles.Echse;
import meineFiles.Papier;
import meineFiles.Schere;
import meineFiles.SchereSteinPapierEchseSpock2PlayerSpielLogik;
import meineFiles.Spiel;
import meineFiles.Spieler;
import meineFiles.Spock;
import meineFiles.Stein;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class SchereSteinPapierEchseSpockLogikTest {

	Spieler mensch, gewinner, computer;
	Spiel testSpiel;
	Auswahl auswahlSchere, auswahlStein, auswahlSpock, auswahlPapier,auswahlEchse;
	SchereSteinPapierEchseSpock2PlayerSpielLogik logik;
	
	@Before
	public void setup(){
		mensch = new Spieler();
		auswahlSchere = new Schere();
		auswahlSpock = new Spock();
		auswahlStein = new Stein();
		auswahlPapier = new Papier();
		auswahlEchse = new Echse();
		mensch.setName("Mario");
	
		computer = new Spieler("Sheldon", false);
		logik = new SchereSteinPapierEchseSpock2PlayerSpielLogik();
	}
	
	@Ignore
	@Test
	public void test() {
		fail("Not yet implemented");
	}
	
	/**
	 *testet R�ckgabewert
	 */
	//unentschieden
	@Test
	public void test01selbeAuswahl(){
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, auswahlSchere, computer, auswahlSchere);
		assertEquals("Spieler 1 und Computer w�hlen beide Schere, Unentschieden!","KEIN_GEWINNER",gewinner.getName());
		
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, auswahlEchse, computer, auswahlEchse);
		assertEquals("Spieler 1 und Computer w�hlen beide Echse, Unentschieden!","KEIN_GEWINNER",gewinner.getName());
		
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, auswahlPapier, computer, auswahlPapier);
		assertEquals("Spieler 1 und Computer w�hlen beide Papier, Unentschieden!","KEIN_GEWINNER",gewinner.getName());
		
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, auswahlSpock, computer, auswahlSpock);
		assertEquals("Spieler 1 und Computer w�hlen beide Spock, Unentschieden!","KEIN_GEWINNER",gewinner.getName());
		
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, auswahlStein, computer, auswahlStein);
		assertEquals("Spieler 1 und Computer w�hlen beide Stein, Unentschieden!","KEIN_GEWINNER",gewinner.getName());
	}
	
	//Spieler 1 gewinnt
	@Test
	public void test02Spieler1gewinnt(){
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, auswahlSchere, computer, auswahlEchse);
		assertEquals("Spieler 1 w�hlt Schere, Computer w�hlt Echse, Spieler 1 gewinnt!","Mario",gewinner.getName());
		
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, auswahlSchere, computer, auswahlPapier);
		assertEquals("Spieler 1 w�hlt Schere, Computer w�hlt Papier, Spieler 1 gewinnt!","Mario",gewinner.getName());
		
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, auswahlStein, computer, auswahlSchere);
		assertEquals("Spieler 1 w�hlt Stein, Computer w�hlt Schere, Spieler 1 gewinnt!","Mario",gewinner.getName());
		
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, auswahlStein, computer, auswahlEchse);
		assertEquals("Spieler 1 w�hlt Stein, Computer w�hlt Echse, Spieler 1 gewinnt!","Mario",gewinner.getName());
		
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, auswahlPapier, computer, auswahlStein);
		assertEquals("Spieler 1 w�hlt Papier, Computer w�hlt Stein, Spieler 1 gewinnt!","Mario",gewinner.getName());
		
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, auswahlPapier, computer, auswahlSpock);
		assertEquals("Spieler 1 w�hlt Papier, Computer w�hlt Spock, Spieler 1 gewinnt!","Mario",gewinner.getName());
		
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, auswahlEchse, computer, auswahlSpock);
		assertEquals("Spieler 1 w�hlt Echse, Computer w�hlt Spock, Spieler 1 gewinnt!","Mario",gewinner.getName());
		
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, auswahlEchse, computer, auswahlPapier);
		assertEquals("Spieler 1 w�hlt Echse, Computer w�hlt Papier, Spieler 1 gewinnt!","Mario",gewinner.getName());
		
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, auswahlSpock, computer, auswahlStein);
		assertEquals("Spieler 1 w�hlt Spock, Computer w�hlt Stein, Spieler 1 gewinnt!","Mario",gewinner.getName());
		
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, auswahlSpock, computer, auswahlSchere);
		assertEquals("Spieler 1 w�hlt Spock, Computer w�hlt Schere, Spieler 1 gewinnt!","Mario",gewinner.getName());
	}
	
	//spieler 1 verliert
	@Test
	public void test03Spieler1verliert(){
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, auswahlSchere, computer, auswahlSpock);
		assertEquals("Spieler 1 w�hlt Schere, Computer w�hlt Spock, Spieler 1 gewinnt!","Mario",gewinner.getName());
		
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, auswahlSchere, computer, auswahlStein);
		assertEquals("Spieler 1 w�hlt Schere, Computer w�hlt Stein, Spieler 1 gewinnt!","Mario",gewinner.getName());
		
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, auswahlStein, computer, auswahlSpock);
		assertEquals("Spieler 1 w�hlt Stein, Computer w�hlt Spock, Spieler 1 gewinnt!","Mario",gewinner.getName());
		
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, auswahlStein, computer, auswahlPapier);
		assertEquals("Spieler 1 w�hlt Stein, Computer w�hlt Papier, Spieler 1 gewinnt!","Mario",gewinner.getName());
		
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, auswahlPapier, computer, auswahlEchse);
		assertEquals("Spieler 1 w�hlt Papier, Computer w�hlt Echse, Spieler 1 gewinnt!","Mario",gewinner.getName());
		
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, auswahlPapier, computer, auswahlSchere);
		assertEquals("Spieler 1 w�hlt Papier, Computer w�hlt Schere, Spieler 1 gewinnt!","Mario",gewinner.getName());
		
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, auswahlEchse, computer, auswahlSchere);
		assertEquals("Spieler 1 w�hlt Echse, Computer w�hlt Schere, Spieler 1 gewinnt!","Mario",gewinner.getName());
		
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, auswahlEchse, computer, auswahlStein);
		assertEquals("Spieler 1 w�hlt Echse, Computer w�hlt Stein, Spieler 1 gewinnt!","Mario",gewinner.getName());
		
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, auswahlSpock, computer, auswahlEchse);
		assertEquals("Spieler 1 w�hlt Spock, Computer w�hlt Echse, Spieler 1 gewinnt!","Mario",gewinner.getName());
		
		gewinner = logik.ermittleGewinnerOhneStringVergleich(mensch, auswahlSpock, computer, auswahlPapier);
		assertEquals("Spieler 1 w�hlt Spock, Computer w�hlt Papier, Spieler 1 gewinnt!","Mario",gewinner.getName());
	}
	
//	//kein Spielername
//	@Test
//	public void test04keinSpieler1Name(){
//		mensch.setName(null);
//		assertEquals("Spieler 1 gibt keinen Namen an (dr�ckt nur Enter), Zur�ck zur Namensabfrage", mensch.getName());
//	}
//	//keine Auswahl getroffen
//		@Test
//		public void test05keineAuswahl(){
//			AuswahlTreffen spielerWaehltNix = new AuswahlTreffen();
//			spielerWaehltNix.calculateSpielerAuswahl(null);
//			assertEquals("Spieler 1 w�hlt nichts",,);
//		}
	

}
